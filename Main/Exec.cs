﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using sortroms.Components;
using Serilog;
using SharpCompress.Archives;
using SharpCompress.Common;
using SharpCompress.Writers;

namespace sortroms.Main
{
    public partial class Program
    {
        public static void Exec()
        {
            // Load JSON file
            var JsonFile = File.ReadAllText(CommandFile);

            // Replace defines by its value
            foreach (var Define in Defines) JsonFile = JsonFile.Replace(Define.Name, Define.Value);
            var Job = JsonConvert.DeserializeObject<Job>(JsonFile);

            // Create logger
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File(Job.LogFile)
                .WriteTo.Console()
                .MinimumLevel.Is(Job.LogLevel)
                .CreateLogger();

            var Index = 0;
#if PARALLEL
		    Parallel.ForEach(Job.RomFiles, RomFile =>
#else            
            foreach (var RomFile in Job.RomFiles)
#endif
            {
                var OutputFile = Job.OutputPath + "/" + RomFile.Name;
                try
                {
                    // Open archive
                    var Archive = ArchiveFactory.Open(RomFile.Filename);
                    foreach (var Entry in Archive.Entries.Where(Entry => Entry.Key == RomFile.Name))
                    {
                        // Extract rom file
                        Log.Verbose($"Extracting {Entry.Key} from {RomFile.Filename} to {Job.OutputPath}");
                        Entry.WriteToDirectory(Job.OutputPath, new ExtractionOptions { ExtractFullPath = true, Overwrite = true });
                    }                    
                }
                catch
                {
                   // It's not an archive
                   File.Copy(RomFile.Filename, OutputFile);
                   Log.Verbose($"Copying {RomFile.Filename} to {OutputFile}");
                }

                // If compression needed
                if (Job.ZipCompressed)
                {
                    var ZipName = Job.OutputPath + "/" + Path.GetFileNameWithoutExtension(RomFile.Name) + ".zip";
                    using (var Zip = File.OpenWrite(ZipName))
                    {
                        using (var ZipWriter = WriterFactory.Open(Zip, ArchiveType.Zip, CompressionType.Deflate))
                        {
                            Log.Verbose($"Zipping {OutputFile} to {ZipName}");
                            var Filename = Path.GetFileName(OutputFile);
                            ZipWriter.Write(Filename, OutputFile);
                        }
                    }

                    // Remove zipped file
                    Log.Verbose($"Deleting {OutputFile}");
                    File.Delete(OutputFile);
                }
                Index++;
                Tools.ShowProgress("Job in progress", Index, Job.RomFiles.Count);
            }
#if PARALLEL
		    );
#endif
        }
    }
}