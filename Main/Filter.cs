﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using sortroms.Components;
using Serilog;

namespace sortroms.Main
{
    partial class Program
    {
        public static void Filter()
        {
            // Load JSON file
            var JsonFile = File.ReadAllText(CommandFile);

            // Replace defines by its value
            foreach (var Define in Defines) JsonFile = JsonFile.Replace(Define.Name, Define.Value);
            Config = JsonConvert.DeserializeObject<Config>(JsonFile);

            // Create logger
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File(Config.Global.Logfile)
                .WriteTo.Console()
                .MinimumLevel.Is(Config.Global.LogLevel)
                .CreateLogger();

            // Load good codes list
            GoodCodes = new GoodCodes(Config.Global.FiltersFile);

            // Retrieve files name
            List<string> InputFiles;
            try
            {
                InputFiles = Directory.EnumerateFiles(Config.InputFiles.Path, Config.InputFiles.SearchPattern, Config.InputFiles.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).ToList();
            }
            catch (Exception E)
            {
                Log.Fatal($"Cannot access {Config.InputFiles.Path}: {E}");
                throw;
            }

            Log.Information($"{InputFiles.Count} files(s) found in {Config.InputFiles.Path}");

            // Retrieve roms files and retrieve the GoodCodes!
            GetAndProcessRomFiles(InputFiles);
            Log.Information($"{RomFiles.Count} rom(s) parsed");

            // Filter the roms
            FilterRoms();

            // Create job
            CreateJob();

        }

        public static void CreateJob()
        {
            var LogFile = Path.GetDirectoryName(Config.Global.Logfile) + "/" + Path.GetFileNameWithoutExtension(Config.Global.Logfile) + "_Job" + Path.GetExtension(Config.Global.Logfile);
            // Create structure
            var Job = new Job
            {
                OutputPath = Config.OutputFiles.Path,
                ZipCompressed = Config.OutputFiles.ZipCompressed,
                LogLevel = Config.Global.LogLevel,
                LogFile = LogFile,
                RomFiles = new ConcurrentBag<Job.RomFile>(),
            };

            // Add roms
#if PARALLEL
            Parallel.ForEach(RomFiles.Where(File => File.Status == RomFileStatus.Elected), RomFile =>
#else
            foreach (var RomFile in RomFiles.Where(File => File.Status == RomFileStatus.Elected))
#endif
                {
                    Job.RomFiles.Add(new Job.RomFile
                    {
                        Filename = RomFile.Filename,
                        Name = RomFile.Name,
                    });
                }
#if PARALLEL
            );
#endif
            // Write JSON
            var Json = JsonConvert.SerializeObject(Job, Formatting.Indented);
            Directory.CreateDirectory(Path.GetDirectoryName(Config.Global.JobJson));
            using (var JsonStream = new StreamWriter(Config.Global.JobJson))
            {
                JsonStream.Write(Json);
            }

            LogFilterResults();
        }
    }
}
