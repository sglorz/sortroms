﻿using System;

namespace sortroms.Main
{
    public partial class Program
    {
        private static void ParseArgs(string[] Args)
        {
            if (Args.Length < 2) Help();

            try
            {
                for (var Arg = 0; Arg < Args.Length; Arg++)
                {
                    if (Args[Arg].ToLower() == "--define" || Args[Arg].ToLower() == "-d")
                    {
                        Arg++;
                        var DefineValue = Args[Arg].Split("=");
                        var Define = new Define {Name = "$" + DefineValue[0].Trim(), Value = DefineValue[1].Trim()};
                        Defines.Add(Define);
                        Console.WriteLine($"Defined {Define.Name}={Define.Value}");
                    }
                    else if (Args[Arg].ToLower() == "filter")
                    {
                        Command = CommandType.Filter;
                        Arg++;
                        CommandFile = Args[Arg];
                    }
                    else if (Args[Arg].ToLower() == "exec")
                    {
                        Command = CommandType.Exec;
                        Arg++;
                        CommandFile = Args[Arg];
                    }
                    else if (Args[Arg].ToLower() == "all")
                    {
                        Command = CommandType.All;
                        Arg++;
                        CommandFile = Args[Arg];
                    }
                    else
                    {
                        Help();
                    }
                }
            }
            catch
            {
                Help();
            }

            if (string.IsNullOrEmpty(CommandFile)) Help();
        }

        private static void Help()
        {
            Console.WriteLine(
                "Usage: sortroms [OPTIONS] COMMAND JSON_FILE\n" +
                "\nSortroms is an UnGoodMerge (http://www.users.on.net/~swcheetah/sam/UnGoodMerge.html) clone with small improvements.\n" +
                "\nCommands are:\n" +
                "  filter\tto sort and filter roms using specified config file\n" +
                "  exec\t\tto execute roms sorting using the job json file resulting form sort command\n" +
                "  all\t\tto execute 'filter' and 'exec' one after the other\n" +
                "\n JSON_FILE is a config file for 'filter' and 'all' or a job file for 'exec'\n" +
                "\nOptions:\n" +
                "  -h, --help\t\t\t\tthis help text\n" +
                "  -d, --define VARIABLE=VALUE\t\tdefines a variable used in config file (beware: case sensitive)" +
                "\n\n\tExample: sortroms -d Platform=amstradcpc filter config.json\n");

            Environment.Exit(0);
        }
    }
}