﻿using System.Linq;
using Serilog;

namespace sortroms.Main
{
    public partial class Program
    {

        public static void LogFilterResults()
        {
            foreach (var RomFile in RomFiles.OrderBy(File => File.Name))
            {
                Log.Information(RomFile.ToString());
            }    
        }
    }
}
