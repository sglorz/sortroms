﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using sortroms.Components;

namespace sortroms.Main
{
    public partial class Program
    {
        /// <summary>
        /// Define structure
        /// </summary>
        public class Define
        {
            public string Name;
            public string Value;
        }

        /// <summary>
        /// Command type
        /// </summary>
        public enum CommandType
        {
            Filter,
            Exec,
            All
        }

        /// <summary>
        /// Command to do from arguments
        /// </summary>
        public static CommandType Command;

        /// <summary>
        /// Command file
        /// </summary>
        public static string CommandFile;
        /// <summary>
        /// All rom files data (thread safe)
        /// </summary>
        public static ConcurrentBag<RomFile> RomFiles;

        /// <summary>
        /// GoodCodes! definitions
        /// </summary>
        public static GoodCodes GoodCodes;

        /// <summary>
        /// Configuration information
        /// </summary>
        public static Config Config;

        /// <summary>
        /// Defines set at application launch
        /// </summary>
        public static readonly List<Define> Defines = new List<Define>();

        public static void Main(string[] Args)
        {
            // AppDomain.CurrentDomain.FirstChanceException += (Sender, EventArgs) => { Log.Fatal(EventArgs.Exception.ToString()); };

            // Get arguments
            ParseArgs(Args);

            if (Command == CommandType.Filter || Command == CommandType.All)
            {
                Filter();
            }

            if (Command == CommandType.All)
            {
                CommandFile = Config.Global.JobJson;
            }

            if (Command == CommandType.Exec || Command == CommandType.All)
            {
                Exec();
            }
        }
    }
}