﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Threading.Tasks;
using sortroms.Components;
using Serilog;

namespace sortroms.Main
{
    public partial class Program
    {
          // Find best rom according to filters
        private static void FilterRoms()
        {
            // Groups of similar roms
            var BrothersList = new ConcurrentDictionary<string, ConcurrentBag<RomFile>>();
#if PARALLEL
		    Parallel.ForEach(RomFiles, (RomFile, State, Index) =>
#else
            var Index = 0;
            foreach (var RomFile in RomFiles)
#endif
            {
                Log.Verbose($"Filtering '{RomFile}'");
                // Filter roms
                VersionStatus(RomFile, Config.VersionStatus);
                OtherFilters(RomFile, Config.Country, nameof(RomFile.Countries));
                OtherFilters(RomFile, Config.Language, nameof(RomFile.Languages));
                OtherFilters(RomFile, Config.NewTranslations, nameof(RomFile.NewTranslations));
                OtherFilters(RomFile, Config.OldTranslations, nameof(RomFile.OldTranslations));
                OtherFilters(RomFile, Config.MainList, nameof(RomFile.MainList));

                // MainList 'None' code filter
                if (RomFile.MainList.Count == 0)
                {
                    if (Config.MainList.Ideal.Contains("None"))
                        RomFile.Status = RomFileStatus.Elected;
                    else if (Config.MainList.Forbidden.Contains("None"))
                        RomFile.Status = RomFileStatus.Eliminated;
                    else if (Config.MainList.NiceToHave.Contains("None"))
                    {
                        var Modifier = Config.MainList.NiceToHave.Count - Config.MainList.NiceToHave.IndexOf("None");
                        RomFile.Votes += Config.MainList.Priority * Modifier;
                    }
                    else if (Config.MainList.BetterNot.Contains("None"))
                    {
                        var Modifier = Config.MainList.BetterNot.Count - Config.MainList.BetterNot.IndexOf("None");
                        RomFile.Votes -= Config.MainList.Priority * Modifier;
                    }
                }

                // Group similar ROMs
                if (!BrothersList.ContainsKey(RomFile.CleanedName))
                    BrothersList.TryAdd(RomFile.CleanedName, new ConcurrentBag<RomFile>());

                BrothersList[RomFile.CleanedName].Add(RomFile);
                Log.Verbose($"Added to group {RomFile.CleanedName}");
#if !PARALLEL
                Index++;
#endif
                Tools.ShowProgress("Selecting roms 1/2:", Index, RomFiles.Count);
            }
#if PARALLEL
		    );
#endif
            // For each groups
#if PARALLEL
		    Parallel.ForEach(BrothersList, (Brothers, State, Index) =>
#else
            Index = 0;
            foreach (var Brothers in BrothersList)
#endif
            {
                Log.Verbose($"Filtering group '{Brothers.Key}'");

                // Keep latest version if configured
                FilterBiggestValue(Brothers.Value, "Version", Config.Version);
                // Keep latest revision if configured
                FilterBiggestValue(Brothers.Value, "Revision", Config.Revision);

               
                Log.Verbose($"Finding best ROMs among '{Brothers.Key}'");
                var BestVotes = int.MinValue;
                var Candidates = new List<RomFile>();

                // Check if a ROM is already elected
                RomFile Elected = null;
                foreach (var Brother in Brothers.Value)
                {
                    if (Brother.Status != RomFileStatus.Elected) continue;
                    if (Elected == null || Elected.Votes < Brother.Votes)
                        Elected = Brother;                    
                }

                // In Highlander mode, if a ROM is already elected, others have lost
                if (Config.FilterMode == Config.FilterModeType.Highlander && Elected != null) 
                {
                    Log.Verbose($"ROM selected: {Elected}");
                    foreach (var RomFile in Brothers.Value)
                    {
                        if (RomFile != Elected) RomFile.Status = RomFileStatus.Eliminated;
                    }
                }
                else
                {
                    // Select the not eliminated roms and retrieve the best vote
                    foreach (var Brother in Brothers.Value.Where(File => File.Status != RomFileStatus.Eliminated))
                    {
                        if (Brother.Votes > BestVotes) BestVotes = Brother.Votes;
                        Candidates.Add(Brother);
                    }

                    // If we have at least one candidate
                    if (Candidates.Count != 0)
                    {
                        // Find the candidates with the best vote
                        foreach (var Candidate in Candidates)
                        {
                            if (Candidate.Votes != BestVotes) continue;
                            Candidate.Status = RomFileStatus.Elected;
                            Log.Verbose($"ROM selected: {Candidate}");
                            // In Highlander mode, first good candidate is enough
                            if (Config.FilterMode == Config.FilterModeType.Highlander) break;
                        }
                    }
                }
#if !PARALLEL
                Index++;
#endif
                Tools.ShowProgress("Selecting roms 2/2:", Index, BrothersList.Count);
            }
#if PARALLEL
		    );
#endif
            // Eliminating pending roms
            foreach (var RomFile in RomFiles.Where(File => File.Status == RomFileStatus.None)) RomFile.Status = RomFileStatus.Eliminated;

            var Winners = RomFiles.Count(File => File.Status == RomFileStatus.Elected);
            var Loosers = RomFiles.Count(File => File.Status == RomFileStatus.Eliminated);

            if (RomFiles.Count(File => File.Status == RomFileStatus.None) > 0)
            {
                var Message = RomFiles.Where(File => File.Status == RomFileStatus.None).Aggregate("", (Current, RomFile) => Current + (RomFile + "\n"));
                throw new Exception($"Found ROM(s) with 'None' as status after filtering:\n{Message}");
            }

            Log.Information($"Filtering results: {Winners} rom(s) kept - {Loosers} rom(s) ignored");
        }

        /// <summary>
        /// Check revision filter
        /// </summary>
        /// <param name="Candidates">Files to test</param>
        /// <param name="RomFileAttribute">RomFile attribute to test</param>
        /// <param name="ConfigValue">Filter configuration value</param>
        private static void FilterBiggestValue(ConcurrentBag<RomFile> Candidates, string RomFileAttribute, bool ConfigValue)
        {
            // If filter is not configured, ignore it
            if (!ConfigValue) return;

            // Find biggest value
            var Biggest = -1;
            var Type = typeof(RomFile);
            var PropInfo = Type.GetProperty(RomFileAttribute);
            foreach (var Candidate in Candidates)
            {
                var Property = (int) PropInfo.GetValue(Candidate, null);
                if (Property > Biggest) 
                    Biggest = Property;
            }

            // None found, nothing to do
            if (Biggest == -1) return;

            // Keep only the files with the biggest value
            foreach (var Candidate in Candidates)
            {
                var Property = (int) PropInfo.GetValue(Candidate, null);
                Candidate.Status = Property == Biggest ? RomFileStatus.Elected : RomFileStatus.Eliminated;
            }
        }

        /// <summary>
        /// Filter on version status
        /// </summary>
        /// <param name="RomFile">Rom file to test</param>
        /// <param name="ExtendedFilters">Config extended filters</param>
        private static void VersionStatus(RomFile RomFile, Config.ExtendedFilters ExtendedFilters)
        {
            if (ExtendedFilters.Forbidden.Contains(RomFile.VersionStatus)) RomFile.Status = RomFileStatus.Eliminated;
            if (ExtendedFilters.Ideal.Contains(RomFile.VersionStatus)) RomFile.Status = RomFileStatus.Elected;
            if (ExtendedFilters.NiceToHave.Contains(RomFile.VersionStatus)) RomFile.Votes += ExtendedFilters.Priority;
            if (ExtendedFilters.BetterNot.Contains(RomFile.VersionStatus)) RomFile.Votes -= ExtendedFilters.Priority;
        }

        /// <summary>
        /// Filter on specified attribute
        /// </summary>
        /// <param name="RomFile">Rom file to test</param>
        /// <param name="ExtendedFilters">Config extended filters</param>
        /// <param name="RomFileAttribute">RomFile attribute name to test</param>
        private static void OtherFilters(RomFile RomFile, Config.ExtendedFilters ExtendedFilters, string RomFileAttribute)
        {       
            var Type = typeof(RomFile);
            var PropInfo = Type.GetProperty(RomFileAttribute);
            var Property = (ConcurrentBag<string>) PropInfo.GetValue(RomFile, null);

            foreach (var Element in ExtendedFilters.Forbidden)
                if (Property.Contains(Element)) 
                    RomFile.Status = RomFileStatus.Eliminated;
            foreach (var Element in ExtendedFilters.Ideal)
                if (Property.Contains(Element)) 
                    RomFile.Status = RomFileStatus.Elected;
            var Modifier = ExtendedFilters.NiceToHave.Count;
            foreach (var Element in ExtendedFilters.NiceToHave)
            {
                if (Property.Contains(Element))
                    RomFile.Votes += ExtendedFilters.Priority * Modifier;
                Modifier--;
            }

            Modifier = ExtendedFilters.BetterNot.Count;
            foreach (var Element in ExtendedFilters.BetterNot)
            {
                if (Property.Contains(Element))
                    RomFile.Votes -= ExtendedFilters.Priority * Modifier;
                Modifier--;
            }
        }
    }
}
