﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using sortroms.Components;
using Serilog;
using SharpCompress.Archives;

namespace sortroms.Main
{
    public partial class Program
    {
      

        /// <summary>
        /// Get rom files and good codes
        /// </summary>
        /// <param name="InputFiles">Input files</param>
        private static void GetAndProcessRomFiles(List<string> InputFiles)
        {
            RomFiles = new ConcurrentBag<RomFile>();

            // Retrieve roms files
            var Current = 0;
#if PARALLEL
            Parallel.ForEach(InputFiles, InputFile =>
#else
            foreach (var InputFile in InputFiles)
#endif
            {
                Log.Verbose($"Managing file {InputFile}");

                IArchive Archive = null;
                try
                {
                    Archive = ArchiveFactory.Open(InputFile);
                }
                catch (Exception)
                {
                    // ignored
                }

                // If it's an archive, all files in archive are added
                if (Archive != null)
                {
                    foreach (var RomFile in from ArchiveEntry in Archive.Entries
                                            where !ArchiveEntry.IsDirectory
                                            select new RomFile
                                            {
                                                Filename = InputFile,
                                                Name = ArchiveEntry.Key
                                            })
                    {
                        // Retrieve rom good codes
                        GoodCodes.Parse(RomFile);
                        RomFiles.Add(RomFile);
                        Log.Verbose($"Found rom: {RomFile}");
                    }
                }
                // otherwise, the file is directly a rom
                else

                {
                    var RomFile = new RomFile {Filename = InputFile, Name = Path.GetFileName(InputFile)};
                    // Retrieve rom good codes
                    GoodCodes.Parse(RomFile);
                    RomFiles.Add(RomFile);
                    Log.Verbose($"Found rom: {RomFile}");
                }

                Current++;
                Tools.ShowProgress("Getting rom info", Current, InputFiles.Count);
            }
#if PARALLEL
             );
#endif
        }
    }
}
