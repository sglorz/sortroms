﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Serilog.Events;

namespace sortroms.Components
{
    public class Config
    {
        public enum FilterModeType
        {
            Highlander, // keep the best ROM that matches the filters, if multipe ROMs matches only the first one is kept
            BandOfBrothers // keep all the ROMs that match the filters
        }

        public class ExtendedFilters
        {
            [JsonProperty("Priority")]
            public int Priority { get; set; }

            [JsonProperty("NiceToHave")]
            public List<string> NiceToHave { get; set; }

            [JsonProperty("BetterNot")]
            public List<string> BetterNot { get; set; }

            [JsonProperty("Ideal")]
            public List<string> Ideal { get; set; }

            [JsonProperty("Forbidden")]
            public List<string> Forbidden { get; set; }
        }

        public class ConfigGlobal
        {
            [JsonProperty("FiltersFile")]
            public string FiltersFile { get; set; }

            [JsonProperty("Logfile")]
            public string Logfile { get; set; }

            [JsonProperty("LogLevel")]
            public LogEventLevel LogLevel { get; set; }

            [JsonProperty("JobJson")]
            public string JobJson { get; set; }
        }


        public class ConfigInputFiles
        {
            [JsonProperty("Path")]
            public string Path { get; set; }

            [JsonProperty("Recursive")]
            public bool Recursive { get; set; }

            [JsonProperty("SearchPattern")]
            public string SearchPattern { get; set; }
        }

        public class ConfigOutputFiles
        {
            [JsonProperty("Path")]
            public string Path { get; set; }

            [JsonProperty("ZipCompressed")]
            public bool ZipCompressed { get; set; }
        }


        public class ScraperOptions
        {
            public string HashFile { get; set; }
            public string OutputFile { get; set; }

            public int DownlaodWorkers { get; set; }
            public int DownloadRetries { get; set; }
            public int MediaWorkers { get; set; }
            
            public bool Append { get; set; }
            public bool Refresh { get; set; }
            public int OverviewLen { get; set; }
            public bool UseFilename { get; set; }
            public bool AddNotFound { get; set; }
            public bool UseNoIntroName { get; set; }

            public bool StripUnicode { get; set; }
            public string Region { get; set; }
            public string Lang { get; set; }

            public bool Mame { get; set; }
            public string MameImg { get; set; }
            public string MameSrcs { get; set; }
            public string ConsoleSrcs { get; set; }
            public string SsUser { get; set; }
            public string SsPassword { get; set; }

            public string ImageDir { get; set; }
            public string ImagePath { get; set; }
            public string ImageSuffix { get; set; }
            public string ThumbSuffix { get; set; }
            public bool ThumbOnly { get; set; }
            public bool NoThumb { get; set; }
            public bool NestedImageDir { get; set; }
            public uint MaxWidth { get; set; }
            public uint MaxHeight { get; set; }
            public bool DownloadImages { get; set; }
            public string ConsoleImg { get; set; }
            public string ImgFormat { get; set; }

            public string MarqueeDir { get; set; }
            public string MarqueePath { get; set; }
            public string MarqueeSuffix { get; set; }
            public bool DownloadMarquees { get; set; }
            public string MarqueeFormat { get; set; }
            
            public string VideoDir { get; set; }
            public string VideoPath { get; set; }
            public string VideoSuffix { get; set; }
            public bool DownloadVideos { get; set; }
            public bool ConvertVideos { get; set; }                                      
        }

        [JsonProperty("Global")]
        public ConfigGlobal Global { get; set; }

        [JsonProperty("InputFiles")]
        public ConfigInputFiles InputFiles { get; set; }

        [JsonProperty("OutputFiles")]
        public ConfigOutputFiles OutputFiles { get; set; }

        [JsonProperty("FilterMode")]
        public FilterModeType FilterMode { get; set; }

        [JsonProperty("Version")]
        public bool Version { get; set; }

        [JsonProperty("Revision")]
        public bool Revision { get; set; }

        [JsonProperty("VersionStatus")]
        public ExtendedFilters VersionStatus { get; set; }

        [JsonProperty("Country")]
        public ExtendedFilters Country { get; set; }

        [JsonProperty("Language")]
        public ExtendedFilters Language { get; set; }

        [JsonProperty("OldTranslations")]
        public ExtendedFilters OldTranslations { get; set; }

        [JsonProperty("NewTranslations")]
        public ExtendedFilters NewTranslations { get; set; }

        [JsonProperty("MainList")]
        public ExtendedFilters MainList { get; set; }

        [JsonProperty("Scraper")]
        public ScraperOptions Scraper { get; set; }
    }
}
