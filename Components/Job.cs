﻿using System.Collections.Concurrent;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Serilog.Events;

namespace sortroms.Components
{
    public class Job
    {
        public class RomFile
        {            
            public string Filename{ get; set; }
            public string Name{ get; set; }
        }
      
        public string OutputPath { get; set; }
        public bool ZipCompressed { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public LogEventLevel LogLevel { get; set; }
        public string LogFile { get; set; }
        public ConcurrentBag<RomFile> RomFiles { get; set; }        
    }
}