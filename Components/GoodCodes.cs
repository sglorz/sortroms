﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using sortroms.Main;

namespace sortroms.Components
{
    /// <summary>
    /// Manages Good codes definitions
    /// </summary>
    public class GoodCodes
    {
        /// <summary>
        /// Codes have a GoodTools name, an internal name and a regex
        /// Each code in Codes class have these 3 values in this order
        /// </summary>
        private const int GOODCODE = 0;
        private const int ID = 1;
        private const int REGEX = 2;
        
        /// <summary>
        /// List all defined codes
        /// </summary>
        private class Codes
        {
            [JsonProperty("MainList")]
            public List<string[]> MainList { get; set; }

            [JsonProperty("VersionCode")]
            public string[] VersionCode { get; set; }

            [JsonProperty("RevisionCode")]
            public string[] RevisionCode { get; set; }

            [JsonProperty("VersionStatus")]
            public List<string[]> VersionStatus { get; set; }

            [JsonProperty("ReleaseYear")]
            public string[] ReleaseYear{ get; set; }

            [JsonProperty("CountryCodes")]
            public List<string[]> CountryCodes { get; set; }

            [JsonProperty("Language")]
            public string[] Language { get; set; }

            [JsonProperty("RecentTranslation")]
            public string[] RecentTranslation { get; set; }
            
            [JsonProperty("OldTranslation")]
            public string[] OldTranslation { get; set; }

            [JsonProperty("LanguagesCodes")]
            public List<string[]> LanguagesCodes { get; set; }            
        }

        /// <summary>
        /// Loaded codes
        /// </summary>
        private readonly Codes _Codes;

        /// <summary>
        /// Constructor
        /// </summary>
        public GoodCodes(string FoodCodesFilename)
        {
            // Load GoodTools codes
            var GoodCodesJson = File.ReadAllText(FoodCodesFilename);
            _Codes = JsonConvert.DeserializeObject<Codes>(GoodCodesJson);
        }

        /// <summary>
        /// Parse a string to retrieve all its codes
        /// </summary>
        /// <param name="RomFile">Rom file data</param>
        public void Parse(RomFile RomFile)
        {
            var Extension = Path.GetExtension(RomFile.Filename);
            RomFile.CleanedName = Path.GetFileNameWithoutExtension(RomFile.Name);

            // Version
            RomFile.CleanedName = RegexMatch(RomFile.CleanedName, _Codes.VersionCode[REGEX], (Sender, Match) =>
            {
                // Change version string value to int
                foreach (var Value in Match.Groups[2].ToString().Split('.')) 
                    RomFile.Version = RomFile.Version * 10 + int.Parse(Value);
            });

            // Revision
            RomFile.CleanedName = RegexMatch(RomFile.CleanedName, _Codes.RevisionCode[REGEX], (Sender, Match) =>
            {
                // Change revision string value to int
                RomFile.Revision = int.Parse(Match.Groups[2].ToString());
            });

            // Version status
            foreach (var CodeVersionStatus in _Codes.VersionStatus)
            {
                RomFile.CleanedName = RegexMatch(RomFile.CleanedName, CodeVersionStatus[REGEX], (Sender, Match) =>
                {
                    // Change revision string value to int
                    RomFile.VersionStatus = CodeVersionStatus[ID];
                });
            }
            
            // Release year
            RomFile.CleanedName = RegexMatch(RomFile.CleanedName, _Codes.ReleaseYear[REGEX], (Sender, Match) =>
            {
                // Change year string value to int
                RomFile.ReleaseYear = Match.Groups[2].ToString();
            });

            // Countries
            foreach (var CountryCode in _Codes.CountryCodes)
            {
                RomFile.CleanedName = RegexMatch(RomFile.CleanedName, CountryCode[REGEX], (Sender, Match) =>
                {
                    RomFile.Countries.Add(CountryCode[ID]);
                });
            }

            // Languages
            var Success = false;
            do
            {
                Success = false;
                RomFile.CleanedName = RegexMatch(RomFile.CleanedName, _Codes.Language[REGEX], (Sender, Match) =>
                {
                    RomFile.Languages.Add(_Codes.Language[ID]);
                    Success = true;
                });
            } while (Success);

            // Recent translations            
            do
            {
                Success = false;
                RomFile.CleanedName = RegexMatch(RomFile.CleanedName, _Codes.RecentTranslation[REGEX], (Sender, Match) =>
                {
                    foreach (var LanguagesCode in _Codes.LanguagesCodes)
                    {
                        if (LanguagesCode[0] != Match.Groups[2].Value) continue;
                        RomFile.NewTranslations.Add(LanguagesCode[1]);
                    }
                    Success = true;
                });
            } while (Success);

            // Old translations            
            do
            {
                Success = false;
                RomFile.CleanedName = RegexMatch(RomFile.CleanedName, _Codes.OldTranslation[REGEX], (Sender, Match) =>
                {
                    foreach (var LanguagesCode in _Codes.LanguagesCodes)
                    {
                        if (LanguagesCode[0] != Match.Groups[2].Value) continue;
                        RomFile.OldTranslations.Add(LanguagesCode[1]);
                    }
                    Success = true;
                });
            } while (Success);

            // Main codes list
            foreach (var Code in _Codes.MainList)
            {
                RomFile.CleanedName = RegexMatch(RomFile.CleanedName, Code[REGEX], (Sender, Match) =>
                {
                    RomFile.MainList.Add(Code[ID]);
                });
            }

            RomFile.CleanedName += Extension;
            RomFile.CleanedName = RomFile.CleanedName.ToLower();
        }

        /// <summary>
        /// Apply regex on string. On success, call OnSuccess and remove match from string.
        /// </summary>
        /// <param name="String">Input string</param>
        /// <param name="RegexString">Regex to apply</param>
        /// <param name="OnSuccess">Event called on success</param>
        /// <returns>Input string without match</returns>
        private string RegexMatch(string String, string RegexString, EventHandler<Match> OnSuccess)
        {
            if (string.IsNullOrEmpty(RegexString)) return String;
            var R = Regex.Match(String, ".*(" + RegexString + ").*");
            if (R.Success)
            {
                OnSuccess.Invoke(this, R);                
                return String.Replace(R.Groups[1].ToString(), "").Trim();
            }

            return String;
        }        
    }
}