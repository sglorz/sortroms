﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace sortroms.Components
{
    public static class Tools
    {
        public static void ClearLine()
        {
            var CurrentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, CurrentLineCursor);
        }

        public static void ShowProgress(string Text, long Current, long Max)
        {
            var Progress = Current / (float) Max;
            var ProgressSize = Console.WindowWidth - 9 - Text.Length;
            var SharpCount = (int) ((ProgressSize) * Progress);
            var Percent = $"{Progress,3:#0%}";
            var ProgressText = $"{Text} {Percent} [{new String('=', SharpCount)}>{new String(' ', ProgressSize - SharpCount)}]";
            Console.Write($"\r{ProgressText}");
        }
    }
}
