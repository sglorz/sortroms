﻿using System.Collections.Concurrent;

namespace sortroms.Components
{
    public enum RomFileStatus
    {
        None,
        Elected,
        Eliminated
    }

    /// <summary>
    /// Rom filename codes
    /// </summary>
    public class RomFile
    {
        public RomFileStatus Status { get; set; }
        public string Filename{ get; set; }
        public string Name{ get; set; }
        public string CleanedName{ get; set; }
        public int Version{ get; set; }
        public int Revision{ get; set; }
        public string VersionStatus{ get; set; }
        public string ReleaseYear{ get; set; }
        public ConcurrentBag<string> Countries { get; set; }
        public ConcurrentBag<string> Languages { get; set; }
        public ConcurrentBag<string> NewTranslations { get; set; }
        public ConcurrentBag<string> OldTranslations { get; set; }
        public ConcurrentBag<string> MainList { get; set; }
        public int Votes { get; set; }

        public RomFile()
        {
            Status = RomFileStatus.None;
            Countries = new ConcurrentBag<string>();
            Languages = new ConcurrentBag<string>();
            NewTranslations = new ConcurrentBag<string>();
            OldTranslations = new ConcurrentBag<string>();
            MainList = new ConcurrentBag<string>();
            Revision = -1;
            Version = -1;
        }

        public override string ToString()
        {
            var S = $"{Name} [{Status}: {Votes}] -";
            if (Version >= 0)  S+= $" V={Version}";
            if (Revision >= 0)  S+= $" R={Revision}";
            if (!string.IsNullOrEmpty(VersionStatus))  S+= $" VS={VersionStatus}";
            if (!string.IsNullOrEmpty(ReleaseYear)) S+= $" RY={ReleaseYear}";
            if (Countries.Count > 0)
            {
                S += " Ctry=";
                foreach (var Country in Countries) S += Country + " ";
            }

            if (Languages.Count > 0)
            {
                S += " Lang=";
                foreach (var Language in Languages) S += Language + " ";
            }

            if (NewTranslations.Count > 0)
            {
                S += " RTrans=";
                foreach (var RecentTranslation in NewTranslations) S += RecentTranslation + " ";
            }

            if (OldTranslations.Count > 0)
            {
                S += " OTrans=";
                foreach (var OldTranslation in OldTranslations) S += OldTranslation + " ";
            }

            if (MainList.Count > 0)
            {
                S += " Codes=";
                foreach (var OtherCode in MainList) S += OtherCode + " ";
            }

            return S;
        }
    } 
}
